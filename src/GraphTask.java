import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (6, 9);

      System.out.println("Input graph:");
      System.out.println (g);

      printSeparator();

      // start of Calculating eccentricity for each vertex of the graph

      System.out.println("Calculating eccentricity for each vertex of the graph " + g.id + "...");
      System.out.println();
      long timerStart = System.currentTimeMillis();
      g.computeEccentricityOfEachVertex();
      System.out.println(g.outputEccentricityOfEachVertex());
      long timerFinish = System.currentTimeMillis();
      long timeElapsed = timerFinish - timerStart;
      System.out.println("Time elapsed to calculate eccentricity for " + g.vertexList.size() + " vertexes: " + timeElapsed + "ms");
      // end of Calculating eccentricity for each vertex of the graph...

      printSeparator();

      /** Ford-Fulkerson algorithm
       *  Objective: Find the maximum flow capacity between two nodes in a graph
       *  Task in estonian:
       *  "On antud sidus lihtgraaf, mille iga serva jaoks on teada selle läbilaskevõime ("toru jämedus", mittenegatiivne)."
       *  "Koostada meetod, mis leiab kahe etteantud tipu vahelise maksimaalse läbilaskevõime Ford-Fulkersoni meetodil."
       *  @author Jan-Erik Kalmus
       */

      System.out.println("Ford-Fulkerson algorithm.\n\n");
      Vertex selected = g.first;
      while(selected.next != null) {
         selected = selected.next;
      }
      FordFulkerson alg = new FordFulkerson(g, g.first, selected);
      alg.execute();

      printSeparator();


      /**
       * @author Aapo Kersalu
       * Prim's algorithm.
       * Objective: Find MST from the graph using Prim's algorithm
       * */
      System.out.println("Find MST using Prim's algorithm: \n \n");
      g.primMST(g.createAdjMatrix());
      Graph mstGraaf = g.constructMstGraph();
      System.out.println(mstGraaf);





   }

   public void printSeparator() {
      System.out.println("\n\n---------------------------------------------------------------------------------------------");
      System.out.println("---------------------------------------------------------------------------------------------\n\n");
   }

   class Vertex implements Comparable<Vertex>{

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      private ArrayList<Integer> adjacentVertexesInfo = new ArrayList<>(); // for Eccentricities

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }


      /** Determines all arcs of Vertex at hand and returns them
       * @return List of all arcs of subject Vertex
       * @author Jan-Erik Kalmus
       */
      private List<Arc> getArcsInVertex() {
         List<Arc> collection = new ArrayList<>();
         Arc subject = this.first;

         do {
            collection.add(subject);
            subject = subject.next;
         } while(subject != null);

         return collection;
      }

      /** Determines whether a specific vertex is connected to this vertex
       * @param target - Vertex expected to be connected with vertex at hand
       * @return Is target vertex connected to this vertex
       * @author Jan-Erik Kalmus
       */
      private boolean vertexInArcs(Vertex target) {
         List<Arc> arcs = this.getArcsInVertex();

         for (Arc arc : arcs) {
            if(arc.target.equals(target) && arc.getCapacity() != 0)
               return true;
         }

         return false;
      }

      /**
       * @author Ardo Rohi
       * Comparator: compares Vertex indexes.
       * Used for sorting Vertexes in TreeMap.
       */
      public int compareTo(Vertex v) {
         return this.info - v.info;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      private final int capacity;
      private int flow = 0;
      private int distance = (int)(Math.random() * ((1000 - 1) + 1)) + 1;

      Arc (String s, Vertex v, Arc a, int c) {
         id = s;
         target = v;
         next = a;
         capacity = c;
      }


      /** Calculates available capacity of the arc
       * @return Current maximum capacity
       * @author Jan-Erik Kalmus
       */
      public int getCapacity() {
         return capacity - flow;
      }

      Arc (String s) {
         this (s, null, null, ThreadLocalRandom.current().nextInt(2, 10 + 1));
      }

      @Override
      public String toString() {
         return id + String.format("(C: %d, F: %d)", capacity, flow);
      }

   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      private Map<Vertex, Integer> vertexEccentricities; // for Eccentricities
      private LinkedList<Vertex> vertexList = new LinkedList<>(); // for Eccentricities
      private MstValues mstValues = null; //for Prim's algorithm

      private final String newLine = System.getProperty ("line.separator"); // For StringBuilder

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         int totalLength = 0;
         String nl = System.getProperty ("line.separator");
         StringBuilder sb = new StringBuilder (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               totalLength += a.distance;
               sb.append (" ");
               sb.append (a.toString());
               sb.append( "|l:" + a.distance + "|"); // Aapo
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         //Aapo
         if (this.mstValues != null) {
            sb.append("Total sum of the distances: " + totalLength/2);
            sb.append(nl);
            sb.append("Longest minimum value between the two cities(d value) for it to work: " + this.mstValues.d);
            sb.append(nl);
         }

         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         vertexList.add(res); // For Eccentricity
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               Arc arc1 = createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               Arc arc2 = createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
               arc1.distance = arc2.distance;
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j] = a.distance;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            Arc arc1 = createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            Arc arc2 = createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            arc1.distance = arc2.distance;
            edgeCount--;  // a new edge happily created
         }
      }

      public Vertex findVertexByInfo(int info) {
         for (Vertex vertex : this.vertexList) {
            if (vertex.info == info) {
               return vertex;
            }
         }
         throw new RuntimeException("No vertex found in this graph with info: " + info);
      }

      /**
       * @author Aapo Kersalu
       * This method goes through all the vertices to find the correct
       * vertice matching the index provided in the method argument
       * @param vertexPos index num of the vertex position we want to get
       * @return Vertex
       */
      public Vertex findVertex(int vertexPos) {
         /**the first vertex in our graph*/
         Vertex v = this.first;
         /**pos of vertex*/
         int counter = 0;
         /**while the vertice is not null*/
         while (v != null){
            if (vertexPos == counter){
               return v;
            }
            v = v.next;
            counter++;
         }
         throw new RuntimeException("Vertex not found: " + vertexPos);
      }

      /**
       * @author Aapo Kersalu
       *  A utility function to find the vertex with minimum key
       *  value, from the set of vertices not yet included in MST
       * @param key Arc values
       * @param mstSet boolean array containing values about vertices whether they are already
       *               processed and in MST or not
       * @return the index value of the vertex with the smallest length to reach
       */
      private int minKey(int key[], Boolean mstSet[])
      {

         /**Initialize min value*/
         int min = Integer.MAX_VALUE, min_index=-1, V = key.length;
         /**Go through all the vertices and pick the smallest key not included in mstSet*/
         for (int v = 0; v < V; v++)
            if (mstSet[v] == false && key[v] < min)
            {
               /**key value*/
               min = key[v];

               /**key index position*/
               min_index = v;
            }
         /**we return the index pos of the smallest arc distance*/
         return min_index;
      }

      /***
       * @author Aapo Kersalu
       * This method creates a MST using the Jarnik-Prim's algorithm. Time Complexity of the below algorithm is O(V^2),
       * we could reduce it to O(E log V) if we were to implement binary heap but our current solution
       * meets the required expectations.
       * @param graph An adjacency matrix representation of a weighted graph
       * @param vertexNames names of the vertices(towns in our case)
       * @return MstValues, an object including all the needed values of the MST
       *
       * Reference to the algorithm I used as a base: https://tutorialspoint.dev/language/
       * c-and-cpp-programs/greedy-algorithms-set-5-prims-minimum-spanning-tree-mst-2
       */
      private MstValues primMST(int graph[][], String[] vertexNames)
      {
         //userInput(graph, vertexNames);

         /**the longest minimum distance between two vertices(especially required by my task 16.23)*/
         int d = 0;
         /**vertice count*/
         int Vcount = graph.length;
         /**Array to store constructed MST*/
         int parent[] = new int[Vcount];

         /**Key values used to pick minimum distance edge in cut*/
         int key[] = new int [Vcount];

         /**To represent set of vertices (not yet) included in MST*/
         Boolean mstSet[] = new Boolean[Vcount];

         /**Initialize all keys as INFINITE*/
         for (int i = 0; i < Vcount; i++)
         {
            key[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
         }

         /**Always include first vertex in MST*/
         key[0] = 0;     /**Make key 0 so that this vertex is picked as first vertex*/
         parent[0] = -1; /**First node is always root of MST*/

         /**
          * The MST will have Vcount vertices
          * Vcount-1 because we don't have to loop through the last row(vertices edges) because we
          * already get all the needed values for it from the all other vertices connections to it.
          */
         for (int count = 0; count < Vcount-1; count++)
         {
            /**
             * Pick the minimum key vertex from the set of vertices
             * not yet included in MST
             */
            int u = minKey(key, mstSet);

            /**Add the picked vertex to the MST Set*/
            mstSet[u] = true;

            /**
             * Update key value and parent index of the adjacent
             *vertices of the picked vertex. Consider only those
             * vertices which are not yet included in MST
             */
            for (int v = 0; v < Vcount; v++){


               /**
                * graph[u][v] is non zero only for adjacent vertices of m!!
                * mstSet[v] is false for vertices not yet included in MST
                * Update the key only if graph[u][v] is smaller than key[v]
                *If we want to change the logic of adjacency then we have to change the 0
                */
               if (graph[u][v]!=0 && mstSet[v] == false && graph[u][v] < key[v])
               {
                  parent[v] = u;
                  key[v] = graph[u][v];
               }
            }
         }
         /**go through the arc lengths array and find the longest distance*/
         for(int i = 0; i < key.length; i++){
            /**if the value at index position of i is bigger than the value in d then assign it to d.*/
            if (d < key[i]) d = key[i];
         }
         this.mstValues = new MstValues(parent, key, d, vertexNames);
         /**return our mstValues*/
         return mstValues;
      }


      /**Method overloading*/
      private MstValues primMST(int[][] graph) {
         return primMST(graph,null);
      }

      /**
       * @author Aapo Kersalu
       * Creates a graph data structure from adjacency matrix (using weighted Edges)
       * @return A fully built new Graph
       */
      private Graph constructMstGraph() {
         /** Build a new graph */
         Graph mstGraph = new Graph("mstG");
         /**lose the reference to previous first vertice(through first vertice we refer to everything else)*/
         //this.first = null;

         /**
          * Creating and adding the vertices:
          * We start from the bottom because createVertex creates a new vertex and adds it as the first one - O(1) complexity (Faster)
          * If we would add it as the last one , then every time we add something we would need go through the vertices one by one
          * through the references to get to the last oneO(n) complexity (Slower)
          * */
         for(int i = mstValues.adjacentVertices.length; i > 0; i--){
            /**if verticeNames == null then create new names, else add vertex name from the array*/
            if (mstValues.vertexNames == null){
               mstGraph.createVertex("vert" + (i - 1)); /**Creates the vertice and names it*/
            } else {
               mstGraph.createVertex("v_" + mstValues.vertexNames[i-1]); /**Creates the vertice and names it*/
            }
         }

         /** Adding the Arcs: Now when we have all the vertices added and named, we add the Arcs*/
         for (int i = 1; i < mstValues.adjacentVertices.length; i++){
            /**get the vertex that is adjacent to i*/
            Vertex vertex1 = mstGraph.findVertex(i); //vertices index (key)
            Vertex vertex2 = mstGraph.findVertex(mstValues.adjacentVertices[i]); //vertices value
            String arcName1_2, arcName2_1;
            /**
             * If we do not have the vertexNames given by the user, name them using the number representation
             * NB! We need to add two Arcs to make a two way connection ( because we have an undirectional graph)
             * */
            if (mstValues.vertexNames == null){
               arcName1_2 = "Arc v" + i + "_" + "v" + mstValues.adjacentVertices[i];
               arcName2_1 = "Arc v" + mstValues.adjacentVertices[i] + "_" + "v" + i;
            } else {
               arcName1_2 = "a_" + mstValues.vertexNames[i] + "-" + mstValues.vertexNames[mstValues.adjacentVertices[i]];
               arcName2_1 = "a_" + mstValues.vertexNames[mstValues.adjacentVertices[i]]+ "-" + mstValues.vertexNames[i];
            }

            /**CREATE THE ARCS*/
            /**Direction1: vertex name, from vertex = i, to vertex = vertices[i]*/
            Arc arc1 = mstGraph.createArc(arcName1_2, vertex1, vertex2);
            /**Direction2: vertex name, to vertex = vertices[i], from vertex = i*/
            Arc arc2 = mstGraph.createArc(arcName2_1, vertex2, vertex1);

            /**add the correct distance value to the arc object under info property*/
            arc1.distance = mstValues.arcValues[i];
            arc2.distance = mstValues.arcValues[i];
         }
         /**Add d value to the new graph*/
         mstGraph.mstValues = new MstValues();
         mstGraph.mstValues.d = mstValues.d;

         return mstGraph;
      }

      /**
       * @author Ardo Rohi
       * Print out eccentricity e(V) for every Vertex in Graph.
       * @return visual representation of eccentricity e(V) for every Vertex in Graph.
       */
      public String outputEccentricityOfEachVertex() {
         StringBuilder output = new StringBuilder();
         output.append("Eccentricities:").append(newLine);

         for (Map.Entry<Vertex,Integer> entry : this.vertexEccentricities.entrySet()){

            output.append("e(")
                    .append(entry.getKey().id)
                    .append(") = ")
                    .append(entry.getValue())
                    .append(newLine);
         }

         return output.toString();
      }


      /**
       * @author Ardo Rohi
       * Print out eccentricity e(V) of given Vertex.
       * @param vertex Vertex object
       * @return visual representation of eccentricity e(V) for given Vertex in Graph.
       */
      public String outputEccentricityOfVertex(Vertex vertex) {
         StringBuilder output = new StringBuilder();

         for (Map.Entry<Vertex,Integer> entry : this.vertexEccentricities.entrySet()){

            if (entry.getKey().equals(vertex)) {
               output.append("e(")
                       .append(entry.getKey().id)
                       .append(") = ")
                       .append(entry.getValue())
                       .append(newLine);
            }
         }

         return output.toString();
      }


      /**
       * @author Ardo Rohi
       * Find Adjacent Vertexes of each Vertex and add adjacent vertex info to List Vertex.adjacentVertexesInfo
       */
      public void computeAdjacentVertexesOfEachVertex() {
         Vertex v = first;
         while (v != null) {
            Arc a = v.first;
            while (a != null) {
               v.adjacentVertexesInfo.add(a.target.info);
               a = a.next;
            }
            v = v.next;
         }
      }

      /**
       * @author Ardo Rohi
       * Construct a new instance of Eccentricity and compute the eccentricity of each vertex in the Graph.
       * Update Map vertexEccentricities
       * Key = Vertex
       * Value = Vertex Eccentricity
       */
      public void computeEccentricityOfEachVertex() {

         if (first == null || vertexList.size() < 1) {
            throw new RuntimeException("Graph \"" + this.id + "\" is empty!");
         }

         computeAdjacentVertexesOfEachVertex();

         Eccentricity eccentricity = new Eccentricity();
         vertexEccentricities = new TreeMap<>();

         for (int vertex = 0; vertex < this.vertexList.size(); vertex++) {
            for (Vertex ver : this.vertexList) {
               if (ver.info == vertex){
                  int vertexEccentricity = eccentricity.breadthFirstSearch(this, ver);
                  vertexEccentricities.put(ver, vertexEccentricity);
               }
            }
         }
      }

   class Eccentricity {


      /**
       * @author Ardo Rohi
       * For each vertex of the graph, the algorithm conducts a breadth first search for all connected vertices.
       * The maximal shortest path from each vertex to any other vertex, the eccentricity of that vertex, is recorded.
       * Also checks whether the eccentricity for the vertex is less than the current radius (smallest eccentricity in the graph)
       * or longer than the current diameter (largest eccentricity in the graph), and updates them if either is true.
       *
       * @param graph the graph to conduct the breadth first search on
       * @param sourceVertex the vertex from which to conduct the search
       * @return int value of sourceVertex Eccentricity.
       */
      private int breadthFirstSearch(Graph graph, Vertex sourceVertex) {
         // Source: https://github.com/ambye85/sedgewick_algorithms/blob/master/src/main/java/uk/ashleybye/sedgewick/graph/Eccentricity.java

         /*
          * Breadth first search queues the source vertex, then iteratively dequeues the vertex and
          * enqueues its adjacent vertices, until no unvisited connected vertices remain. Each vertex is
          * marked as visited the first time it is encountered, and a distance to the source vertex
          * is recorded.
          */
         Queue<Integer> queue = new Queue<>();
         boolean[] isVisited = new boolean[graph.vertexList.size()];
         int[] distanceToSource = new int[graph.vertexList.size()];

         isVisited[sourceVertex.info] = true;
         distanceToSource[sourceVertex.info] = 0;
         queue.enqueue(sourceVertex.info);

         int eccentricity = 0;
         while (!queue.isEmpty()) {

            int thisVertexInfo = queue.dequeue();

            ArrayList<Integer> adjacentVertexesInfo = new ArrayList<>();

            for (Vertex vertex : graph.vertexList) {
               if (vertex.info == thisVertexInfo) {
                  adjacentVertexesInfo = vertex.adjacentVertexesInfo;
               }
            }

            if (adjacentVertexesInfo.size() <= 0 || adjacentVertexesInfo == null) {
               throw new RuntimeException("No adjacent vertexes found for vertex " + graph.findVertexByInfo(thisVertexInfo));
            }

            for (int adjacentVertex : adjacentVertexesInfo) {
               if (!isVisited[adjacentVertex]) {
                  distanceToSource[adjacentVertex] = distanceToSource[thisVertexInfo] + 1;
                  isVisited[adjacentVertex] = true;
                  queue.enqueue(adjacentVertex);

                  if (distanceToSource[adjacentVertex] > eccentricity) {
                     eccentricity = distanceToSource[adjacentVertex];
                  }
               }
            }
         }

         return eccentricity;
      }

   }

   /**
    * @author Ardo Rohi
    * Queue helps Breadth first search queue the source vertex, then iteratively dequeue the vertex and
    * enqueue its adjacent vertices.
    */
   class Queue<T> implements Iterable<T> {
// Source https://github.com/ambye85/sedgewick_algorithms/blob/master/src/main/java/uk/ashleybye/sedgewick/collections/Queue.java

      private Node front;
      private Node back;
      private int nodeCount;

      private class Node {

         T item;
         Node next;
      }

      public boolean isEmpty() {
         return front == null;
      }

      public int size() {
         return nodeCount;
      }

      public void enqueue(T item) {
         Node temp = back;
         back = new Node();
         back.item = item;
         back.next = null;

         if (isEmpty()) {
            front = back;
         } else {
            temp.next = back;
         }

         nodeCount++;
      }

      public T dequeue() {
         T item = front.item;
         front = front.next;

         if (isEmpty()) {
            back = null;
         }

         nodeCount--;

         return item;
      }

      @Override
      public Iterator<T> iterator() {
         return new ListIterator();
      }

      private class ListIterator implements Iterator<T> {

         private Node current = front;

         @Override
         public boolean hasNext() {
            return current != null;
         }

         @Override
         public T next() {
            T item = current.item;
            current = current.next;
            return item;
         }

         @Override
         public void remove() {
         }
      }

   }

   }

   /** Ford-Fulkerson algorithm processing class
    *  Contains alrogithm rep
    * @author Jan-Erik Kalmus
    */
   class FordFulkerson {

      private final Vertex start;
      private final Vertex sink;
      private final Graph graph;
      private Path currentPath;


      /** Constructor for Ford-Fulkerson algorithm processor
       * @param graph - Subject graph for algorithm
       * @param source - Source node of Ford-Fulkerson algorithm
       * @param sink - Sink node of Ford-Fulkerson algorithm
       */

      public FordFulkerson(Graph graph, Vertex source, Vertex sink) {
         this.start = source;
         this.sink = sink;
         this.graph = graph;
      }


      private class Path {
         private final LinkedList<Arc> arcs;
         private int maximumCapacity;

         /**
          * Supporting class to designate an augmented path in graph
          */

         public Path () {
            arcs = new LinkedList<>();
            maximumCapacity = Integer.MAX_VALUE;
         }

         /**
          * Add an arc to the current path
          */
         public void pushArc (Arc addition) {
            arcs.add(addition);
            recalculateArcCapacity();
         }

         /**
          *  Recalculates maximum capacity of the path based on maximum capacities of
          *  all of the arcs present in the path.
          */
         private void recalculateArcCapacity() {
            for(Arc arc : arcs) {
               if(arc.getCapacity() < this.maximumCapacity) {
                  this.maximumCapacity = arc.getCapacity();
               }
            }
         }

         /**
          *  Updates the flows of all the arcs present in the path according to the bottleneck value
          * @throws IllegalStateException - An arc does not have the required capacity for the predetermined paths bottleneck
          */

         private void updateFlows() {
            for(Arc arc : arcs) {
               if(arc.getCapacity() - this.maximumCapacity < 0) {
                  String errorMessage = String.format("Error in updating capacity for arc: %s. Arc capacity: %d. Path capacity: %d", arc.toString(), arc.getCapacity(), this.maximumCapacity);
                  throw new IllegalStateException(errorMessage);
               }

               arc.flow += this.maximumCapacity;
            }
         }

         /**
          *  Getter method for maximum capacity of path
          *  @return Maximum capacity of the path
          */

         public int getMaximumCapacity () {
            return maximumCapacity;
         }

         /**
          * Builds string representation of the path from starting node to the final node
          * @return String representation of path
          */

         public String toString() {
            StringBuilder builder = new StringBuilder();

            Iterator<Arc> iterator =  arcs.iterator();

            while(iterator.hasNext()) {
               builder.append(iterator.next().toString());

               if(iterator.hasNext()) {
                  builder.append(" -> ");
               }
            }

            builder.append(String.format(" Path capacity: %d", maximumCapacity));

            return builder.toString();
         }
      }

      /**
       * Calculate maximum flow between two random points using Ford-Fulkerson algorithm
       * Ford-Fulkerson algorithm:
       * - Determine whether an legitimate path to destination exists
       * - Fetch maximum flow of this path - ultimately determined by its bottleneck capacity
       * - Form a sum of said flows
       * @return maximum flow capacity between designated source and sink
       */
      public int execute() {
         // Initialize maximum flow
         int maximumFlow = 0;

         System.out.println("Source node: " + this.start.toString());
         System.out.println("Destination node: " + this.sink.toString());

         // Loop while we have legitimate paths
         while(findPath()) {

            // Update capacities of all arcs in the path according to its bottleneck
            currentPath.updateFlows();

            System.out.println("Traversed path: " + currentPath.toString());

            // Append maxmimum flow of current path
            maximumFlow += currentPath.getMaximumCapacity();
         }

         System.out.println("Maximum capacity: " + maximumFlow);
         // Return result
         return maximumFlow;
      }


      /**
       * Supporting class for BFS queue
       * Keeps reference of subject Vertex and the previous Vertex associated to it
       */
      private class QueueItem {
         public Vertex previous;
         public Vertex subject;

         public QueueItem(Vertex p, Vertex s) {
            previous = p;
            subject = s;
         }
      }

      /**
       * Find and process an untraversed path. Determine bottleneck of this path in process
       * @return path found
       */
      private boolean findPath () {

         // Initialize queues and traversed vertices list
         LinkedList<QueueItem> queue = new LinkedList<QueueItem>();
         List<Vertex> traversedVertices = new ArrayList<Vertex>();
         List<Vertex> previous = new ArrayList<Vertex>();

         // Start from the beginning
         queue.add(new QueueItem(this.start, this.start));

         // BFS
         while(!queue.isEmpty()) {
            QueueItem qItem = queue.poll();
            Vertex subject = qItem.subject;
            boolean connectedToDestination = subject.vertexInArcs(this.sink);

            // Continue if we have explored this vertex already
            if(traversedVertices.contains(subject)) {
               continue;
            }

            traversedVertices.add(subject);
            previous.add(qItem.previous);

            List<Arc> arcs = subject.getArcsInVertex();

            // Check the neighbors of current vertex
            for (Arc arc : arcs) {

               // Skip if arc lacks capacity -> Can not go there
               if(arc.getCapacity() == 0) {
                  continue;
               }

               // This vertex has a valid path to the sink node
               if(connectedToDestination && !arc.target.equals(sink)) {
                  continue;
               }

               // If we have found a path to the subject, finish
               if(arc.target.equals(sink)) {
                  traversedVertices.add(arc.target);
                  previous.add(subject);
                  return path_found(traversedVertices, previous);
               }

               // If the target of this arc has not been discovered yet, add to queue
               if(!traversedVertices.contains(arc.target)) {
                  queue.add(new QueueItem(subject, arc.target));
               }
            }
         }

         return path_found(traversedVertices, previous);
      }

      /** Attempts to reconstruct road based on BFS result, ultimately determines whether connection exists
       * @param v - list of traversed vertices
       * @param p - list of previous vertices associated with traversed vertices at equal list index
       * @return Evaluation of legitimate path from source to sink being found
       */
      public boolean path_found(List<Vertex> v, List<Vertex> p) {
         // Initialize currentPath
         currentPath = new Path();

         // To keep track of path while backtracking from end
         List<Arc> reversePath = new ArrayList<>();

         // If the relation between the amount of vertices and previous vertices is not correct, path was not found
         if(v.size() != p.size()) {
            return false;
         }

         // The loop did not break at the final vertex
         if (!v.get(v.size() - 1).equals(this.sink)) {
            return false;
         }

         // Final vertex --> Expectedly the sink
         Vertex current = v.get(v.size() - 1);

         // Start backtracking the path
         for (int i = v.size() - 1; i >= 0; i--) {
            Vertex previous = p.get(i);

            // Can only happen with the first one
            if(previous.equals(current)) {
               break;
            }

            if(!previous.vertexInArcs(current)) {
               return false;
            }

            boolean found = false;

            // Find our suitable arc and add it
            for (Arc arc : previous.getArcsInVertex()) {
               if(arc.target.equals(current)) {
                  reversePath.add(arc);
                  found = true;
                  current = previous;

                  i = v.indexOf(current) + 1; // Point index at correct location
               }
            }

            // If a suitable arc was not found,
            if(!found) {
               return false;
            }
         }

         // Reverse the reversed paths list
         Collections.reverse(reversePath);

         // Construct path
         for(Arc arc : reversePath) {
            currentPath.pushArc(arc);
         }

         return true;
      }

   }

   /**
    * @author Aapo Kersalu
    * This class is made for returning multiple PRIM's Minimal Spanning Tree values easily to make methods more versatile
    */
   class MstValues {
      private int[] adjacentVertices;
      private int[] arcValues;
      private int d = 0;
      private String[] vertexNames;


      MstValues (int[] adjacentVertices, int[] arcValues, int distance, String[] names){
         // I do not actually need to write this keyword
         this.adjacentVertices = adjacentVertices;
         this.arcValues = arcValues;
         d = distance;
         vertexNames = names;

      }

      /**
       * For overloading if we do not provide names for the vertices
       * @param adjacentVertices - adjacent vertices
       * @param arcValues - corresponding arc lengths for the adjacent vertices
       */
      MstValues (int[] adjacentVertices, int[] arcValues, int distance) {
         this (adjacentVertices, arcValues, distance, null);
      }

      MstValues () {
         this (null, null, 0, null);
      }
   }

}

